// bài tập 1

function baiTap1() {
  var cot = "";
  for (var i = 0; i < 10; i++) {
    var hang = "";
    for (var j = 1; j <= 10; j++) {
      hang += `<td> ${i * 10 + j}  </td>`;
    }
    cot += `<tr> ${hang} </tr>`;
    document.getElementById(
      "result1"
    ).innerHTML = `<table style="width:100%"> ${cot} </table>`;
  }
}
var arr = [];
function baiTap2() {
  var numArr = +document.getElementById("txt-numArr1").value;
  arr.push(numArr);
  document.getElementById("result2").innerHTML = `<h2>[${arr}]</h2>`;
  document.getElementById("txt-numArr1").value = "";
}

function baiTap3() {
  var S = 0;
  var soN = document.getElementById("txt-so-n-bt3").value * 1;
  if (soN < 2) {
    document.getElementById(
      "result3"
    ).innerHTML = `<h2> Nhập lại số n phải lớn hơn 2 </h2>`;
  } else {
    for (var i = 2; i <= soN; i++) {
      S += i;
    }
    document.getElementById("result3").innerHTML = `<h2> Tổng: ${
      S + 2 * soN
    } </h2>`;
  }
}

function baiTap4() {
  var ketQua = "";
  var soN = document.getElementById("txt-so-n-bt4").value * 1;
  for (var i = soN; i > 0; i--) {
    if (soN % i == 0) {
      ketQua += i + "," + " ";
    }
  }
  document.getElementById(
    "result4"
  ).innerHTML = `<h2>Ước số của ${soN} là: ${ketQua} </h2>`;
}

function baiTap5() {
  var soN = document.getElementById("txt-so-n-bt5").value * 1;
  chuoiSoN = soN.toString();
  var chuoiDuocTach = chuoiSoN.split("");
  var chuoiDuocDao = chuoiDuocTach.reverse();
  var chuoiDuocGop = chuoiDuocDao.join("");
  document.getElementById(
    "result5"
  ).innerHTML = `<h2> Chuỗi mới được đảo là: ${chuoiDuocGop} </h2>`;
}

function baiTap6() {
  var sum = 0;
  var count = 0;
  for (var i = 1; sum <= 100; i++) {
    sum += i;
    count = i - 1;
  }
  document.getElementById(
    "result6"
  ).innerHTML = `<h2>Giá trị x nguyên dương là: ${count}</h2>`;
}

function baiTap7() {
  var content = "";
  var n = document.getElementById("txt-so-n-bt7").value * 1;
  for (var i = 0; i <= 10; i++) {
    content += `<h2>${n} x ${i} = ${n * i}</h2>`;
  }
  document.getElementById("result7").innerHTML = content;
}

function baiTap8() {
  var player1 = [];
  var player2 = [];
  var player3 = [];
  var player4 = [];
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  for (var i = 0; i <= 11; i++) {
    var indexPlayer = (i + 1) % 4;
    if (indexPlayer == 1) {
      player1.push(cards[i]);
    }
    if (indexPlayer == 2) {
      player2.push(cards[i]);
    }
    if (indexPlayer == 3) {
      player3.push(cards[i]);
    }
    if (indexPlayer == 0) {
      player4.push(cards[i]);
    }
  }
  document.getElementById("result8").innerHTML = `<h2>Player1: ${player1}</h2>
  <h2>Player2: ${player2}</h2>
  <h2>Player3: ${player3}</h2>
  <h2>Player4: ${player4}</h2>`;
}

function baiTap9() {
  var tongSoCon = document.getElementById("txt-so-con").value * 1;
  var tongSoChan = document.getElementById("txt-so-chan").value * 1;
  /**
   * Ta có:
   * Tổng số chân = 2 x gà + 4 x chó
   * Tổng số con =      gà +     chó
   * Vậy số con chó = (Tổng số chân  - 2xTổng số con)/2
   */
  var soConCho = (tongSoChan - tongSoCon * 2) / 2;
  var soConGa = tongSoCon - soConCho;
  document.getElementById(
    "result9"
  ).innerHTML = `<h2>Tổng số gà là: ${soConGa}, Tổng số chó là: ${soConCho}</h2>`;
}

function baiTap10() {
  var gio = document.getElementById("txt-gio").value * 1;
  var phut = document.getElementById("txt-phut").value * 1;
  if (gio < 0 || gio >= 24 || phut < 0 || phut >= 60) {
    document.getElementById("result10").innerHTML = `<h2>Nhập lại</h2>`;
  } else {
    var gocTaoBoiKimGio = ((gio * 60 + phut) * 720) / (24 * 60);
    var gocTaoBoiKimPhut = (phut * 360) / 60;
    if (gocTaoBoiKimGio >= 0 && gocTaoBoiKimGio < 360) {
      var gocLech = Math.abs(gocTaoBoiKimGio - gocTaoBoiKimPhut);
    } else {
      gocLech = Math.abs(gocTaoBoiKimGio - 360 - gocTaoBoiKimPhut);
    }
    if (gocLech > 180) {
      gocLech = 360 - gocLech;
    }
    document.getElementById(
      "result10"
    ).innerHTML = `<h2>Góc lệch là: ${gocLech.toLocaleString()} độ</h2>`;
  }
}
